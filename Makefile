all: push build deploy

push:
	git push

build:
	rm -rf dist
	API_URL=https://api.lumi.partidopirata.com.ar \
		npm run build

deploy:
	rsync -rvP dist/ root@partidopirata.com.ar:/srv/http/lumi.partidopirata.com.ar/
