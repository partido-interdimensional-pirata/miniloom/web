self.addEventListener('message', messageEvent => {
  if (messageEvent.data === 'skipWaiting') return skipWaiting()
})

self.addEventListener('push', event => {
  const payload = event.data && event.data.text()
  if (!payload)
    return console.error('Hubo un problema con una notificación!', event)
  const json = JSON.parse(payload)
  event.waitUntil(
    self.registration.showNotification(json.subject, {
      body: json.message,
    }),
  )
})
