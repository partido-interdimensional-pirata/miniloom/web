import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import { registerWebpush } from './sjwManager'

Vue.use(Vuex)

const API_URL = process.env.API_URL || 'http://localhost:3000'

const generateToken = (
  email = localStorage.email,
  password = localStorage.password,
) => btoa(email + ':' + password)

export const request = async (path, settings = {}) => {
  if (settings.parseJson === undefined) settings.parseJson = true
  if (settings.handleStatusCode === undefined) settings.handleStatusCode = true

  let args = { headers: {} }

  if (settings.json) {
    args.headers['Content-Type'] = 'application/json'
    args.body = JSON.stringify(settings.json)
  }

  if (settings.authenticated || typeof settings.authenticated === 'undefined') {
    args.headers['Authorization'] = `Basic ${generateToken()}`
  }
  if (settings.authentication) {
    const { email, password } = settings.authentication
    args.headers['Authorization'] = `Basic ${generateToken(email, password)}`
  }

  args = {
    ...args,
    ...settings,
    headers: { ...args.headers, ...(settings.headers || {}) },
  }

  const response = await fetch(API_URL + path, args)

  if (settings.handleStatusCode) {
    if (response.status === 403) {
      throw 'forbidden'
    }
    if (response.status === 404) {
      throw 'not_found'
    }
    if (response.status >= 500) {
      throw 'internal_server_error'
    }
    if (response.status === 422) {
      let error
      try {
        const json = await response.json()
        if (json.errors) {
          if (json.errors[0]) error = json.errors[0]
          // si es un array
          else error = json.errors
        } else throw null
      } catch (error) {
        error = 'unprocessable_entity'
      }
      throw error
    }
  }

  if (settings.parseJson) {
    return await response.json()
  } else {
    return response
  }
}

const store = new Vuex.Store({
  plugins: [
    createPersistedState({
      paths: ['consensos', 'barcas'],
    }),
  ],
  state: {
    loggingIn: true,
    initialLoadDone: false,
    loggedIn: false,
    profile: null,
    actualizacionDisponible: false,

    consensos: [],
    barcas: [],
  },
  mutations: {
    loggedIn: state => (state.loggedIn = true),
    loggedOut: state => {
      state.loggedIn = false
      state.profile = null
      state.consensos = []
      state.barcas = []
    },

    startedLoggingIn: state => (state.loggingIn = true),
    finishedLoggingIn: state => {
      state.loggingIn = false
      state.initialLoadDone = true
    },

    gotProfile: (state, profile) => (state.profile = profile),

    actualizacionDisponible: state => (state.actualizacionDisponible = true),

    gotBarcas: (state, barcas) => {
      state.barcas = barcas
    },
    gotBarca: (state, barcaNueva) => {
      state.barcas = [
        ...state.barcas.filter(barca => barca.id != barcaNueva.id),
        barcaNueva,
      ]
    },

    abordadaBarca: (state, barcaId) => {
      const barca = state.barcas.find(barca => barca.id === barcaId)
      if (barca) {
        state.barcas = [
          ...state.barcas.filter(barca => barca.id !== barcaId),
          { ...barca, abordada: true },
        ]
      }
    },
    abandonadaBarca: (state, barcaId) => {
      state.barcas = state.barcas.filter(barca => barca.id !== barcaId)
    },

    gotConsensos: (state, { barcaId, consensos }) => {
      state.consensos = [
        ...state.consensos.filter(consenso => consenso.barca_id != barcaId),
        ...consensos.map(consenso => ({
          ...consenso,
          barca_id: barcaId,
        })),
      ]
    },
    gotConsenso: (state, { barcaId, consenso: nuevoConsenso }) => {
      state.consensos = [
        ...state.consensos.filter(consenso => consenso.id != nuevoConsenso.id),
        { ...nuevoConsenso, barca_id: barcaId },
      ]
    },
    deletedConsenso: (state, { barcaId, id }) => {
      state.consensos = state.consensos.filter(consenso => consenso.id != id)
    },

    gotPosicion: (state, { consensoId, posicion }) => {
      const consenso = state.consensos.find(
        consenso => consenso.id == consensoId,
      )
      if (consenso) {
        consenso.posiciones = [...(consenso.posiciones || []), posicion]
      }
      // TODO XXX FIX que las posiciones no se actualizan
    },
  },
  actions: {
    async logIn({ commit, dispatch }, { email, password }) {
      commit('startedLoggingIn')

      try {
        const json = await request('/piratas/yo', {
          authentication: { email, password },
        })
        localStorage.email = email
        localStorage.password = password

        commit('gotProfile', json)
        commit('loggedIn')
      } catch (error) {
        if (error === 'forbidden') {
          // asegurarnos de anular los datos
          dispatch('logOut')
        }
        throw error
      }
      commit('finishedLoggingIn')

      registerWebpush()
    },
    logOut({ commit }) {
      localStorage.removeItem('email')
      localStorage.removeItem('password')
      commit('loggedOut')
      commit('finishedLoggingIn')
    },
    async crearCuenta({ commit, dispatch }, userData) {
      const json = await request('/piratas', {
        method: 'POST',
        json: { pirata: userData },
        authenticated: false,
      })
      if (json.errors) throw json.errors
      commit('gotProfile', json)
      return await dispatch('logIn', {
        email: userData.email,
        password: userData.password,
      })
    },

    async conseguirBarca({ commit }, { barcaId }) {
      const json = await request(`/barcas/${barcaId}`)
      return commit('gotBarca', json)
    },
    async conseguirTodasLasBarcas({ commit }) {
      const json = await request(`/barcas/todas`)
      return commit('gotBarcas', json.barcas)
    },
    async conseguirBarcasAbordadas({ commit }) {
      const json = await request(`/barcas`)
      return commit('gotBarcas', json.barcas)
    },

    async abordarBarca({ commit }, { barcaId }) {
      const res = await request(`/barcas/${barcaId}/abordar`, {
        method: 'PUT',
        parseJson: false,
      })
      commit('abordadaBarca', barcaId)
    },
    async abandonarBarca({ commit }, { barcaId }) {
      const res = await request(`/barcas/${barcaId}/abandonar`, {
        method: 'DELETE',
        parseJson: false,
      })
      commit('abandonadaBarca', barcaId)
    },
    async crearBarca({ commit }, body) {
      const json = await request(`/barcas`, {
        method: 'POST',
        json: body,
        handleStatusCode: false,
      })
      if (json.errors) throw json.errors
      return json
    },

    async conseguirConsensos({ commit }, { barcaId }) {
      const json = await request(`/barcas/${barcaId}/consensos`)
      return commit('gotConsensos', { barcaId, consensos: json.consensos })
    },
    async conseguirConsenso({ commit }, { consensoId, barcaId }) {
      const json = await request(`/barcas/${barcaId}/consensos/${consensoId}`)
      return commit('gotConsenso', { barcaId, consenso: json })
    },

    async crearConsenso({ commit }, { consenso, barcaId }) {
      const json = await request(`/barcas/${barcaId}/consensos`, {
        method: 'POST',
        json: { consenso },
      })
      if (json.errors) throw json.errors
      commit('gotConsenso', { barcaId, consenso: json })
      return json
    },
    // esto retorna el consenso eliminado para poder re-escribirlo o similar
    async eliminarConsenso({ commit }, { barcaId, consensoId }) {
      const json = await request(`/barcas/${barcaId}/consensos/${consensoId}`, {
        method: 'DELETE',
      })
      if (json.errors) throw json.errors
      commit('deletedConsenso', { barcaId, id: consensoId })
      return json
    },

    async crearPosicion({ commit }, { barcaId, consensoId, posicion }) {
      const json = await request(
        `/barcas/${barcaId}/consensos/${consensoId}/posiciones`,
        {
          method: 'POST',
          json: { posicion },
        },
      )
      if (json.errors) throw json.errors
      commit('gotPosicion', { consensoId, barcaId, posicion: json })
      return json
    },
  },
})

if (localStorage.email && localStorage.password) {
  store.dispatch('logIn', {
    email: localStorage.email,
    password: localStorage.password,
  })
} else {
  store.dispatch('logOut')
}

export default store
